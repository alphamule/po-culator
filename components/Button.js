import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  min-height: 0vw;
  width: clamp(55px, calc(3.4375rem + ((1vw - 3.2px) * 15.2083)), 128px);
  height: clamp(55px, calc(3.4375rem + ((1vw - 3.2px) * 15.2083)), 128px);
  box-sizing: border-box;
  padding: clamp(4px, calc(0.25rem + ((1vw - 3.2px) * 0.8333)), 8px);

  border-radius: 4px;

  font-size: clamp(11px, calc(0.6875rem + ((1vw - 3.2px) * 1.875)), 20px);

  line-height: 1.1;
  text-align: center;
  hyphens: auto;

  border: 1px solid white;
  color: white;
`;

const Contents = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
`;

const Label = styled.div`
  text-align: center;
  width: 100%;
  border-top: 1px solid #DDDDDD;
  color: #DDDDDD;
  padding-top: 1px;
`;

export default function Button(props) {
  return (
    <Wrapper {...props}>
      <Contents>{props.children}</Contents>
      <Label>{props.label}</Label>
    </Wrapper>
  );
}