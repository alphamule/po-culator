import styled from "styled-components";
import React from "react";
import KeyPad from './KeyPad';
import Button from './Button';
import ClickableButton from './ClickableButton';
import { Mode } from './PORefData';


const Wrapper = styled.div`
  display: grid;
  margin-top: 16px;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: repeat(5, 1fr);
  gap: 8px;
  grid-template-areas:
    "sound pattern bpm a b"
    "key key key key funct"
    "key key key key fx"
    "key key key key play"
    "key key key key write"
`;

export default class ButtonGrid extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mode: Mode.NONE,
    };
  }

  toggle(newMode) {
    this.setState({ mode: this.state.mode === newMode ? Mode.NONE : newMode });
  }

  render() {
    const selected = this.props.selected;
    const operations = selected.operations[this.state.mode];
    const {
      pattern = "",
      bpm = "",
      keys = [],
      funct = "",
      write = "",
      a = "",
      b = "",
      play = "",
    } = operations ? operations : {};
    const labels = selected.labels || {};

    return (
      <Wrapper>
        <ClickableButton
          onClick={() => this.toggle(Mode.SOUND)}
          buttonMode={Mode.SOUND}
          currentMode={this.state.mode}
          label={"sound"}
          style={{ gridArea: "sound" }}
        />
        <ClickableButton
          onClick={() => this.toggle(Mode.PATTERN)}
          buttonMode={Mode.PATTERN}
          currentMode={this.state.mode}
          label={"pattern"}
          style={{ gridArea: "pattern" }}
        >
          {pattern}
        </ClickableButton>
        <ClickableButton
          onClick={() => this.toggle(Mode.BPM)}
          buttonMode={Mode.BPM}
          currentMode={this.state.mode}
          label={"bpm"}
          style={{ gridArea: "bpm" }}
        >
          {bpm}
        </ClickableButton>
        <Button style={{ gridArea: "a" }} label={"a"}>
          {a}
        </Button>
        <Button style={{ gridArea: "b" }} label={"b"}>
          {b}
        </Button>
        <Button style={{ gridArea: "funct" }} label={labels.funct || "funct"}>
          {funct}
        </Button>
        <ClickableButton
          onClick={() => this.toggle(Mode.FUNCT)}
          buttonMode={Mode.FUNCT}
          currentMode={this.state.mode}
          label={labels.funct || "funct"}
          style={{ gridArea: "funct" }}
        >
          {funct}
        </ClickableButton>
        <ClickableButton
          style={{ gridArea: "fx" }}
          onClick={() => this.toggle(Mode.FX)}
          buttonMode={Mode.FX}
          currentMode={this.state.mode}
          label={labels.fx || "FX"}
        ></ClickableButton>
        <Button style={{ gridArea: "play" }} label={"play"}>
          {play}
        </Button>
        <ClickableButton
          onClick={() => this.toggle(Mode.WRITE)}
          buttonMode={Mode.WRITE}
          currentMode={this.state.mode}
          label={"write"}
          style={{ gridArea: "write" }}
        >
          {write}
        </ClickableButton>
        <KeyPad style={{ gridArea: "key" }} operations={keys} />
      </Wrapper>
    );
  }
}