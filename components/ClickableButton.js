import styled from 'styled-components';
import Button from './Button';


const ActiveWrapper = styled(Button)`
  border-color: red;
`;
const ClickableWrapper = styled(Button)`
  border-color: mediumseagreen;
`;

export default function ClickableButton(props) {
  let { buttonMode, currentMode } = props;
  let Wrapper = ClickableWrapper;
  if (buttonMode === currentMode) {
    Wrapper = ActiveWrapper;
  }
  return <Wrapper {...props}>{props.children}</Wrapper>;
}