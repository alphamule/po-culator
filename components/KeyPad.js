import Button from './Button';
import styled from "styled-components";

const Wrapper = styled.div`
  grid-area: key;
  position: relative;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: inherit;
`;

const Overlay = styled.div`
  position: absolute;
  left: 0px;
  right: 0px;
  top: 0px;
  bottom: 0px;
  border-radius: 4px;

  border: 1px solid white;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const OverlayContent = styled.div`
  color: white;
  width: 75%;
  font-size: clamp(11px, calc(0.6875rem + ((1vw - 3.2px) * 1.875)), 20px);
`;

export default function KeyPad({operations}) {
  let overlayShowing = false;
  if (operations.length != 16 && operations.length != 0) {
    overlayShowing = true;
  }

  return (
    <Wrapper>
      {overlayShowing ? (
        <Overlay>
          {operations.map((o) => (
            <OverlayContent>{o}</OverlayContent>
          ))}
        </Overlay>
      ) : (
        <>
          <Button label={"1"}>{operations[0]}</Button>
          <Button label={"2"}>{operations[1]}</Button>
          <Button label={"3"}>{operations[2]}</Button>
          <Button label={"4"}>{operations[3]}</Button>
          <Button label={"5"}>{operations[4]}</Button>
          <Button label={"6"}>{operations[5]}</Button>
          <Button label={"7"}>{operations[6]}</Button>
          <Button label={"8"}>{operations[7]}</Button>
          <Button label={"9"}>{operations[8]}</Button>
          <Button label={"10"}>{operations[9]}</Button>
          <Button label={"11"}>{operations[10]}</Button>
          <Button label={"12"}>{operations[11]}</Button>
          <Button label={"13"}>{operations[12]}</Button>
          <Button label={"14"}>{operations[13]}</Button>
          <Button label={"15"}>{operations[14]}</Button>
          <Button label={"16"}>{operations[15]}</Button>
        </>
      )}
    </Wrapper>
  );
}