import styled from 'styled-components';

const Wrapper = styled.button`
  color: white;
  background-color: var(--color);

  font-size: 11px;

  border: ${p => p.selected.id == p.button.id ? '1px solid red' : 'none' };
  font-family: 'Oswald', Tahoma, sans-serif;
  width: 64px;
  height: 20px;
  border-radius: 2px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 0px;
  line-height: 1;
`;

export default function POButton({button, onClick, selected}) {
  return (
    <Wrapper
      style={{ "--color": button.color }}
      onClick={() => onClick(button)}
      selected={selected}
      button={button}
    >
      PO-{button.id}
    </Wrapper>
  );
}