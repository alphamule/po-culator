import styled from 'styled-components';
import POButton from './POButton';
import { poRefData } from './PORefData'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 8px;
  flex-wrap: wrap;
  gap: 4px;
  align-self: flex-end;
`;

const GenWrapper = styled.div`
  display: flex;
  flex-direction: row;
  gap: 4px;
`;

export default function POGrid({onClick, selected}) {
  let gen1 = poRefData.filter((b) => b.generation === 1);
  let gen2 = poRefData.filter((b) => b.generation === 2);
  let gen3 = poRefData.filter((b) => b.generation === 3);

  return (
    <Wrapper>
      <GenWrapper>
        {gen1.map((button) => (
          <POButton
            key={button.id}
            onClick={onClick}
            button={button}
            selected={selected}
          />
        ))}
      </GenWrapper>
      <GenWrapper>
        {gen2.map((button) => (
          <POButton
            key={button.id}
            onClick={onClick}
            button={button}
            selected={selected}
          />
        ))}
      </GenWrapper>
      <GenWrapper>
        {gen3.map((button) => (
          <POButton
            key={button.id}
            onClick={onClick}
            button={button}
            selected={selected}
          />
        ))}
      </GenWrapper>
    </Wrapper>
  );
}