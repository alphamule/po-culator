import React from 'react';
import styled from 'styled-components';
import Head from 'next/head';
import POGrid from '../components/POGrid';
import ButtonGrid from '../components/ButtonGrid';
import { poRefData } from '../components/PORefData'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  font-family: "Oswald", Tahoma, sans-serif;
`;

const Link = styled.a`
  color: #CCCCCC;
  font-size: clamp(11px, calc(0.6875rem + ((1vw - 3.2px) * 1.875)), 20px);
  margin-top: 16px;
  align-self: flex-end;
`;

const InnerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Title = styled.div`
  font-size: 36px;
  color: white;
  align-self: flex-start;
  line-height: 1.1;
  margin-top: 8px;

  position: relative;
  left: -2px;

  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

const Number = styled.div``;
const Name = styled.div`
  color: #CCCCCC;
`;

const Tagline = styled.div`
  font-size: clamp(11px, calc(0.6875rem + ((1vw - 3.2px) * 1.875)), 20px);
  color: #CCCCCC;
  align-self: flex-start;
`;

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: poRefData[0],
    }
  }

  handleClick(button) {
    this.setState({ 'selected': button })
  }

  render() {
    const { selected } = this.state;

    return (
      <Wrapper>
        <Head>
          <style>{`html { background-color: ${selected.color}; }`}</style>
        </Head>
        <InnerWrapper>
          <Title><Number>PO-{selected.id}</Number><Name>{selected.name}</Name></Title>
          <Tagline>{selected.tagline}</Tagline>
          <ButtonGrid selected={selected} />
          <Link href={selected.url}>{selected.url}</Link>
          <POGrid
            onClick={(button) => this.handleClick(button)}
            selected={selected}
          />
        </InnerWrapper>
      </Wrapper>
    );
  }
}
